// Grammar file for the Jolene document language

const COMMENT = token(/;;.*/)

module.exports = grammar({
  name: 'jolene',

  extras: $ => [/(\s|\f)/, $.comment],

  rules: {
    document: $ => repeat($.block_segment),

    comment: $ => COMMENT,

    segment_open_mark: $ => '[',
    segment_close_mark: $ => ']',

    block_segment: $ => seq(
      $.segment_open_mark,
      $.segment_content,
      $.segment_close_mark
    ),

    inline_segment: $ => seq(
      $.segment_open_mark,
      $.segment_content,
      $.segment_close_mark
    ),

    segment_content: $ => prec.left(repeat1(
      choice(
        $.text,
        $.text_escape,
        $.inline_segment
      )
    )),

    text_escape: $ => token.immediate(
      seq(
        '\\',
        choice(
          '[',
          ']'
        )
      )
    ),

    text: $ => token.immediate(/(\s|[^\\\[\]])+/),

  }
});
