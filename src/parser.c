#include <tree_sitter/parser.h>

#if defined(__GNUC__) || defined(__clang__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmissing-field-initializers"
#endif

#define LANGUAGE_VERSION 13
#define STATE_COUNT 13
#define LARGE_STATE_COUNT 6
#define SYMBOL_COUNT 12
#define ALIAS_COUNT 0
#define TOKEN_COUNT 6
#define EXTERNAL_TOKEN_COUNT 0
#define FIELD_COUNT 0
#define MAX_ALIAS_SEQUENCE_LENGTH 3
#define PRODUCTION_ID_COUNT 1

enum {
  sym_comment = 1,
  sym_segment_open_mark = 2,
  sym_segment_close_mark = 3,
  sym_text_escape = 4,
  sym_text = 5,
  sym_document = 6,
  sym_block_segment = 7,
  sym_inline_segment = 8,
  sym_segment_content = 9,
  aux_sym_document_repeat1 = 10,
  aux_sym_segment_content_repeat1 = 11,
};

static const char * const ts_symbol_names[] = {
  [ts_builtin_sym_end] = "end",
  [sym_comment] = "comment",
  [sym_segment_open_mark] = "segment_open_mark",
  [sym_segment_close_mark] = "segment_close_mark",
  [sym_text_escape] = "text_escape",
  [sym_text] = "text",
  [sym_document] = "document",
  [sym_block_segment] = "block_segment",
  [sym_inline_segment] = "inline_segment",
  [sym_segment_content] = "segment_content",
  [aux_sym_document_repeat1] = "document_repeat1",
  [aux_sym_segment_content_repeat1] = "segment_content_repeat1",
};

static const TSSymbol ts_symbol_map[] = {
  [ts_builtin_sym_end] = ts_builtin_sym_end,
  [sym_comment] = sym_comment,
  [sym_segment_open_mark] = sym_segment_open_mark,
  [sym_segment_close_mark] = sym_segment_close_mark,
  [sym_text_escape] = sym_text_escape,
  [sym_text] = sym_text,
  [sym_document] = sym_document,
  [sym_block_segment] = sym_block_segment,
  [sym_inline_segment] = sym_inline_segment,
  [sym_segment_content] = sym_segment_content,
  [aux_sym_document_repeat1] = aux_sym_document_repeat1,
  [aux_sym_segment_content_repeat1] = aux_sym_segment_content_repeat1,
};

static const TSSymbolMetadata ts_symbol_metadata[] = {
  [ts_builtin_sym_end] = {
    .visible = false,
    .named = true,
  },
  [sym_comment] = {
    .visible = true,
    .named = true,
  },
  [sym_segment_open_mark] = {
    .visible = true,
    .named = true,
  },
  [sym_segment_close_mark] = {
    .visible = true,
    .named = true,
  },
  [sym_text_escape] = {
    .visible = true,
    .named = true,
  },
  [sym_text] = {
    .visible = true,
    .named = true,
  },
  [sym_document] = {
    .visible = true,
    .named = true,
  },
  [sym_block_segment] = {
    .visible = true,
    .named = true,
  },
  [sym_inline_segment] = {
    .visible = true,
    .named = true,
  },
  [sym_segment_content] = {
    .visible = true,
    .named = true,
  },
  [aux_sym_document_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_segment_content_repeat1] = {
    .visible = false,
    .named = false,
  },
};

static const TSSymbol ts_alias_sequences[PRODUCTION_ID_COUNT][MAX_ALIAS_SEQUENCE_LENGTH] = {
  [0] = {0},
};

static const uint16_t ts_non_terminal_alias_map[] = {
  0,
};

static bool ts_lex(TSLexer *lexer, TSStateId state) {
  START_LEXER();
  eof = lexer->eof(lexer);
  switch (state) {
    case 0:
      if (eof) ADVANCE(4);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\f' ||
          lookahead == '\r' ||
          lookahead == ' ') ADVANCE(11);
      if (lookahead == ';') ADVANCE(10);
      if (lookahead == '[') ADVANCE(6);
      if (lookahead == '\\') ADVANCE(2);
      if (lookahead == ']') ADVANCE(7);
      if (lookahead != 0) ADVANCE(12);
      END_STATE();
    case 1:
      if (lookahead == ';') ADVANCE(5);
      END_STATE();
    case 2:
      if (lookahead == '[' ||
          lookahead == ']') ADVANCE(8);
      END_STATE();
    case 3:
      if (eof) ADVANCE(4);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\f' ||
          lookahead == '\r' ||
          lookahead == ' ') SKIP(3)
      if (lookahead == ';') ADVANCE(1);
      if (lookahead == '[') ADVANCE(6);
      if (lookahead == ']') ADVANCE(7);
      END_STATE();
    case 4:
      ACCEPT_TOKEN(ts_builtin_sym_end);
      END_STATE();
    case 5:
      ACCEPT_TOKEN(sym_comment);
      if (lookahead != 0 &&
          lookahead != '\n') ADVANCE(5);
      END_STATE();
    case 6:
      ACCEPT_TOKEN(sym_segment_open_mark);
      END_STATE();
    case 7:
      ACCEPT_TOKEN(sym_segment_close_mark);
      END_STATE();
    case 8:
      ACCEPT_TOKEN(sym_text_escape);
      END_STATE();
    case 9:
      ACCEPT_TOKEN(sym_text);
      if (lookahead == '\n') ADVANCE(12);
      if (lookahead != 0 &&
          (lookahead < '[' || ']' < lookahead)) ADVANCE(9);
      if (('[' <= lookahead && lookahead <= ']')) ADVANCE(5);
      END_STATE();
    case 10:
      ACCEPT_TOKEN(sym_text);
      if (lookahead == ';') ADVANCE(9);
      if (lookahead != 0 &&
          (lookahead < '[' || ']' < lookahead)) ADVANCE(12);
      END_STATE();
    case 11:
      ACCEPT_TOKEN(sym_text);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\f' ||
          lookahead == '\r' ||
          lookahead == ' ') ADVANCE(11);
      if (lookahead == ';') ADVANCE(10);
      if (lookahead != 0 &&
          (lookahead < '[' || ']' < lookahead)) ADVANCE(12);
      END_STATE();
    case 12:
      ACCEPT_TOKEN(sym_text);
      if (lookahead != 0 &&
          (lookahead < '[' || ']' < lookahead)) ADVANCE(12);
      END_STATE();
    default:
      return false;
  }
}

static const TSLexMode ts_lex_modes[STATE_COUNT] = {
  [0] = {.lex_state = 0},
  [1] = {.lex_state = 3},
  [2] = {.lex_state = 0},
  [3] = {.lex_state = 0},
  [4] = {.lex_state = 0},
  [5] = {.lex_state = 0},
  [6] = {.lex_state = 3},
  [7] = {.lex_state = 3},
  [8] = {.lex_state = 0},
  [9] = {.lex_state = 3},
  [10] = {.lex_state = 3},
  [11] = {.lex_state = 3},
  [12] = {.lex_state = 3},
};

static const uint16_t ts_parse_table[LARGE_STATE_COUNT][SYMBOL_COUNT] = {
  [0] = {
    [ts_builtin_sym_end] = ACTIONS(1),
    [sym_comment] = ACTIONS(3),
    [sym_segment_open_mark] = ACTIONS(1),
    [sym_segment_close_mark] = ACTIONS(1),
    [sym_text_escape] = ACTIONS(1),
    [sym_text] = ACTIONS(1),
  },
  [1] = {
    [sym_document] = STATE(10),
    [sym_block_segment] = STATE(6),
    [aux_sym_document_repeat1] = STATE(6),
    [ts_builtin_sym_end] = ACTIONS(5),
    [sym_comment] = ACTIONS(7),
    [sym_segment_open_mark] = ACTIONS(9),
  },
  [2] = {
    [sym_inline_segment] = STATE(4),
    [sym_segment_content] = STATE(11),
    [aux_sym_segment_content_repeat1] = STATE(4),
    [sym_comment] = ACTIONS(3),
    [sym_segment_open_mark] = ACTIONS(11),
    [sym_text_escape] = ACTIONS(13),
    [sym_text] = ACTIONS(15),
  },
  [3] = {
    [sym_inline_segment] = STATE(4),
    [sym_segment_content] = STATE(12),
    [aux_sym_segment_content_repeat1] = STATE(4),
    [sym_comment] = ACTIONS(3),
    [sym_segment_open_mark] = ACTIONS(11),
    [sym_text_escape] = ACTIONS(13),
    [sym_text] = ACTIONS(15),
  },
  [4] = {
    [sym_inline_segment] = STATE(5),
    [aux_sym_segment_content_repeat1] = STATE(5),
    [sym_comment] = ACTIONS(3),
    [sym_segment_open_mark] = ACTIONS(11),
    [sym_segment_close_mark] = ACTIONS(17),
    [sym_text_escape] = ACTIONS(19),
    [sym_text] = ACTIONS(21),
  },
  [5] = {
    [sym_inline_segment] = STATE(5),
    [aux_sym_segment_content_repeat1] = STATE(5),
    [sym_comment] = ACTIONS(3),
    [sym_segment_open_mark] = ACTIONS(23),
    [sym_segment_close_mark] = ACTIONS(26),
    [sym_text_escape] = ACTIONS(28),
    [sym_text] = ACTIONS(31),
  },
};

static const uint16_t ts_small_parse_table[] = {
  [0] = 4,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(9), 1,
      sym_segment_open_mark,
    ACTIONS(34), 1,
      ts_builtin_sym_end,
    STATE(7), 2,
      sym_block_segment,
      aux_sym_document_repeat1,
  [14] = 4,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(36), 1,
      ts_builtin_sym_end,
    ACTIONS(38), 1,
      sym_segment_open_mark,
    STATE(7), 2,
      sym_block_segment,
      aux_sym_document_repeat1,
  [28] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(43), 1,
      sym_text_escape,
    ACTIONS(41), 3,
      sym_segment_open_mark,
      sym_segment_close_mark,
      sym_text,
  [40] = 2,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(45), 2,
      ts_builtin_sym_end,
      sym_segment_open_mark,
  [48] = 2,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(47), 1,
      ts_builtin_sym_end,
  [55] = 2,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(49), 1,
      sym_segment_close_mark,
  [62] = 2,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(51), 1,
      sym_segment_close_mark,
};

static const uint32_t ts_small_parse_table_map[] = {
  [SMALL_STATE(6)] = 0,
  [SMALL_STATE(7)] = 14,
  [SMALL_STATE(8)] = 28,
  [SMALL_STATE(9)] = 40,
  [SMALL_STATE(10)] = 48,
  [SMALL_STATE(11)] = 55,
  [SMALL_STATE(12)] = 62,
};

static const TSParseActionEntry ts_parse_actions[] = {
  [0] = {.entry = {.count = 0, .reusable = false}},
  [1] = {.entry = {.count = 1, .reusable = false}}, RECOVER(),
  [3] = {.entry = {.count = 1, .reusable = false}}, SHIFT_EXTRA(),
  [5] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_document, 0),
  [7] = {.entry = {.count = 1, .reusable = true}}, SHIFT_EXTRA(),
  [9] = {.entry = {.count = 1, .reusable = true}}, SHIFT(2),
  [11] = {.entry = {.count = 1, .reusable = false}}, SHIFT(3),
  [13] = {.entry = {.count = 1, .reusable = true}}, SHIFT(4),
  [15] = {.entry = {.count = 1, .reusable = false}}, SHIFT(4),
  [17] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_segment_content, 1),
  [19] = {.entry = {.count = 1, .reusable = true}}, SHIFT(5),
  [21] = {.entry = {.count = 1, .reusable = false}}, SHIFT(5),
  [23] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_segment_content_repeat1, 2), SHIFT_REPEAT(3),
  [26] = {.entry = {.count = 1, .reusable = false}}, REDUCE(aux_sym_segment_content_repeat1, 2),
  [28] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_segment_content_repeat1, 2), SHIFT_REPEAT(5),
  [31] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_segment_content_repeat1, 2), SHIFT_REPEAT(5),
  [34] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_document, 1),
  [36] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_document_repeat1, 2),
  [38] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_document_repeat1, 2), SHIFT_REPEAT(2),
  [41] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_inline_segment, 3),
  [43] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_inline_segment, 3),
  [45] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_block_segment, 3),
  [47] = {.entry = {.count = 1, .reusable = true}},  ACCEPT_INPUT(),
  [49] = {.entry = {.count = 1, .reusable = true}}, SHIFT(9),
  [51] = {.entry = {.count = 1, .reusable = true}}, SHIFT(8),
};

#ifdef __cplusplus
extern "C" {
#endif
#ifdef _WIN32
#define extern __declspec(dllexport)
#endif

extern const TSLanguage *tree_sitter_jolene(void) {
  static const TSLanguage language = {
    .version = LANGUAGE_VERSION,
    .symbol_count = SYMBOL_COUNT,
    .alias_count = ALIAS_COUNT,
    .token_count = TOKEN_COUNT,
    .external_token_count = EXTERNAL_TOKEN_COUNT,
    .state_count = STATE_COUNT,
    .large_state_count = LARGE_STATE_COUNT,
    .production_id_count = PRODUCTION_ID_COUNT,
    .field_count = FIELD_COUNT,
    .max_alias_sequence_length = MAX_ALIAS_SEQUENCE_LENGTH,
    .parse_table = &ts_parse_table[0][0],
    .small_parse_table = ts_small_parse_table,
    .small_parse_table_map = ts_small_parse_table_map,
    .parse_actions = ts_parse_actions,
    .symbol_names = ts_symbol_names,
    .symbol_metadata = ts_symbol_metadata,
    .public_symbol_map = ts_symbol_map,
    .alias_map = ts_non_terminal_alias_map,
    .alias_sequences = &ts_alias_sequences[0][0],
    .lex_modes = ts_lex_modes,
    .lex_fn = ts_lex,
  };
  return &language;
}
#ifdef __cplusplus
}
#endif
