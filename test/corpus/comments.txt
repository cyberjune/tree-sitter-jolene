================
Comments
================

;; a comment
[and a block] ;; and another

;; and another comment

----------------

(document
  (comment)
  (block_segment
    (segment_open_mark)
    (segment_content
      (text))
    (segment_close_mark))
  (comment)
  (comment))


================
Semi-colon in text
================

;; a comment
[a text block with a semi-colon (;) character]

;; and another comment

----------------

(document
  (comment)
  (block_segment
    (segment_open_mark)
    (segment_content
      (text))
    (segment_close_mark))
  (comment))


================
Double Semi-colon in text
================

;; a comment
[a text block with a double semi-colon (;;) sequence]

;; and another comment

----------------

(document
  (comment)
  (block_segment
    (segment_open_mark)
    (segment_content
      (text))
    (segment_close_mark))
  (comment))
